﻿using Microsoft.Extensions.Configuration;
using SecurifyAPI.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SecurifyAPI.Data
{
    public class VisitaRepository
    {
        private readonly string _connectionString;

        public VisitaRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("defaultConnection");
        }

        public async Task<List<Visita>> GetVisitas()
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_getVisitas", connection);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            var response = new List<Visita>();
            try
            {
                await connection.OpenAsync();
                var reader = await cmd.ExecuteReaderAsync();
                while(await reader.ReadAsync())
                {
                    response.Add(MapToVisita(reader));
                }
            }
            catch (Exception ex)
            {
                
            }
            return response;
        }

        public async Task<Visita> AgregarVisita(Visita visita)
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_AgregarVisita", connection);
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();
            var response = new Visita();
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idUsuario", visita.idUsuario);
            cmd.Parameters.AddWithValue("@idVisitante", visita.idVisitante);
            cmd.Parameters.AddWithValue("@idDomicilio", visita.idDomicilio);
            cmd.Parameters.AddWithValue("@idTipo", visita.idTipo);
            try
            {
                await connection.OpenAsync();
                adapter.SelectCommand = cmd;
                adapter.Fill(dataSet);

                response = new Visita()
                {
                    id = (int)dataSet.Tables[0].Rows[0].ItemArray[0],
                    idUsuario = (int)dataSet.Tables[0].Rows[0].ItemArray[1],
                    idVisitante = (int)dataSet.Tables[0].Rows[0].ItemArray[2],
                    idDomicilio = (int)dataSet.Tables[0].Rows[0].ItemArray[3],
                    idTipo = (int)dataSet.Tables[0].Rows[0].ItemArray[4],
                    fecha = (DateTime)dataSet.Tables[0].Rows[0].ItemArray[5]
                };
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        private Visita MapToVisita(SqlDataReader reader)
        {
            return new Visita()
            {
                id = (int)reader["id"],
                idUsuario = (int)reader["idUsuario"],
                idVisitante = (int)reader["idVisitante"],
                idDomicilio = (int)reader["idDomicilio"],
                idTipo = (int)reader["idTipo"],
                fecha = (DateTime)reader["fecha"]
            };
        }
    }
}
