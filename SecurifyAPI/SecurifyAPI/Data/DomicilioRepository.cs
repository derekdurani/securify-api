﻿using Microsoft.Extensions.Configuration;
using SecurifyAPI.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SecurifyAPI.Data
{
    public class DomicilioRepository
    {
        private readonly string _connectionString;

        public DomicilioRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("defaultConnection");
        }

        public async Task<List<Domicilio>> GetDomicilios()
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_getDomicilios", connection);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            var response = new List<Domicilio>();
            try
            {
                await connection.OpenAsync();
                var reader = await cmd.ExecuteReaderAsync();
                while (await reader.ReadAsync())
                {
                    response.Add(MapToDomicilio(reader));
                }
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        public async Task<Domicilio> GetDomicilioID(Domicilio domicilio)
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_getDomicilioID", connection);
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", domicilio.id);
            var response = new Domicilio();
            try
            {
                await connection.OpenAsync();
                adapter.SelectCommand = cmd;
                adapter.Fill(dataSet);
                response = new Domicilio()
                {
                    id = (int)dataSet.Tables[0].Rows[0].ItemArray[0],
                    idCalle = (int)dataSet.Tables[0].Rows[0].ItemArray[1],
                    numero = dataSet.Tables[0].Rows[0].ItemArray[2].ToString(),
                    tipo = dataSet.Tables[0].Rows[0].ItemArray[3].ToString()
                };
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        public async Task<Domicilio> AgregarDomicilio(Domicilio domicilio)
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_agregarDomicilio", connection);
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();
            var response = new Domicilio();
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idCalle", domicilio.idCalle);
            cmd.Parameters.AddWithValue("@numero", domicilio.numero);
            cmd.Parameters.AddWithValue("@tipo", domicilio.tipo);
            try
            {
                await connection.OpenAsync();
                adapter.SelectCommand = cmd;
                adapter.Fill(dataSet);

                response = new Domicilio()
                {
                    id = (int)dataSet.Tables[0].Rows[0].ItemArray[0],
                    idCalle = (int)dataSet.Tables[0].Rows[0].ItemArray[1],
                    numero = dataSet.Tables[0].Rows[0].ItemArray[2].ToString(),
                    tipo = dataSet.Tables[0].Rows[0].ItemArray[3].ToString()
                };
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        public async Task<Domicilio> ActualizarDomicilio(Domicilio domicilio)
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_actualizarDomicilio", connection);
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();
            var response = new Domicilio();
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", domicilio.id);
            cmd.Parameters.AddWithValue("@idCalle", domicilio.idCalle);
            cmd.Parameters.AddWithValue("@numero", domicilio.numero);
            cmd.Parameters.AddWithValue("@tipo", domicilio.tipo);
            try
            {
                await connection.OpenAsync();
                adapter.SelectCommand = cmd;
                adapter.Fill(dataSet);

                response = new Domicilio()
                {
                    id = (int)dataSet.Tables[0].Rows[0].ItemArray[0],
                    idCalle = (int)dataSet.Tables[0].Rows[0].ItemArray[1],
                    numero = dataSet.Tables[0].Rows[0].ItemArray[2].ToString(),
                    tipo = dataSet.Tables[0].Rows[0].ItemArray[3].ToString()
                };
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        private Domicilio MapToDomicilio(SqlDataReader reader)
        {
            return new Domicilio()
            {
                id = (int)reader["id"],
                idCalle = (int)reader["idCalle"],
                numero = reader["numero"].ToString(),
                tipo = reader["tipo"].ToString()
            };
        }
    }
}
