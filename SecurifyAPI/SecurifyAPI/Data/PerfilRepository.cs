﻿using Microsoft.Extensions.Configuration;
using SecurifyAPI.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SecurifyAPI.Data
{
    public class PerfilRepository
    {
        private readonly string _connectionString;

        public PerfilRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("defaultConnection");
        }

        public async Task<List<Perfil>> GetPerfiles()
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_getPerfiles", connection);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            var response = new List<Perfil>();
            try
            {
                await connection.OpenAsync();
                var reader = await cmd.ExecuteReaderAsync();
                while (await reader.ReadAsync())
                {
                    response.Add(MapToPerfil(reader));
                }
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        public async Task<Perfil> AgregarPerfil(Perfil perfil)
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_AgregarPerfil", connection);
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();
            var response = new Perfil();
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@nombre", perfil.nombre);
            try
            {
                await connection.OpenAsync();
                adapter.SelectCommand = cmd;
                adapter.Fill(dataSet);

                response = new Perfil()
                {
                    id = (int)dataSet.Tables[0].Rows[0].ItemArray[0],
                    nombre = dataSet.Tables[0].Rows[0].ItemArray[1].ToString()
                };
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        public async Task<Perfil> ActualizarPerfil(Perfil perfil)
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_actualizarPerfil", connection);
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();
            var response = new Perfil();
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", perfil.id);
            cmd.Parameters.AddWithValue("@nombre", perfil.nombre);
            try
            {
                await connection.OpenAsync();
                adapter.SelectCommand = cmd;
                adapter.Fill(dataSet);

                response = new Perfil()
                {
                    id = (int)dataSet.Tables[0].Rows[0].ItemArray[0],
                    nombre = dataSet.Tables[0].Rows[0].ItemArray[1].ToString()
                };
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        private Perfil MapToPerfil(SqlDataReader reader)
        {
            return new Perfil()
            {
                id = (int)reader["id"],
                nombre = reader["nombre"].ToString()
            };
        }
    }
}
