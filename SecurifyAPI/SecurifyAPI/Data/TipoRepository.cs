﻿using Microsoft.Extensions.Configuration;
using SecurifyAPI.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SecurifyAPI.Data
{
    public class TipoRepository
    {
        private readonly string _connectionString;

        public TipoRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("defaultConnection");
        }

        public async Task<List<Tipo>> GetTipos()
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_getTipos", connection);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            var response = new List<Tipo>();
            try
            {
                await connection.OpenAsync();
                var reader = await cmd.ExecuteReaderAsync();
                while (await reader.ReadAsync())
                {
                    response.Add(MapToTipo(reader));
                }
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        public async Task<Tipo> AgregarTipo(Tipo tipo)
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_agregarTipo", connection);
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();
            var response = new Tipo();
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@nombre", tipo.nombre);
            try
            {
                await connection.OpenAsync();
                adapter.SelectCommand = cmd;
                adapter.Fill(dataSet);

                response = new Tipo()
                {
                    id = (int)dataSet.Tables[0].Rows[0].ItemArray[0],
                    nombre = dataSet.Tables[0].Rows[0].ItemArray[1].ToString()
                };
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        public async Task<Tipo> ActualizarTipo(Tipo tipo)
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_actualizarTipo", connection);
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();
            var response = new Tipo();
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", tipo.id);
            cmd.Parameters.AddWithValue("@nombre", tipo.nombre);
            try
            {
                await connection.OpenAsync();
                adapter.SelectCommand = cmd;
                adapter.Fill(dataSet);

                response = new Tipo()
                {
                    id = (int)dataSet.Tables[0].Rows[0].ItemArray[0],
                    nombre = dataSet.Tables[0].Rows[0].ItemArray[1].ToString()
                };
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        private Tipo MapToTipo(SqlDataReader reader)
        {
            return new Tipo()
            {
                id = (int)reader["id"],
                nombre = reader["nombre"].ToString()
            };
        }
    }
}
