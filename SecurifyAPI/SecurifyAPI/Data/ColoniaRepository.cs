﻿using Microsoft.Extensions.Configuration;
using SecurifyAPI.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SecurifyAPI.Data
{
    public class ColoniaRepository
    {
        private readonly string _connectionString;

        public ColoniaRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("defaultConnection");
        }

        public async Task<List<Colonia>> GetColonias()
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_getColonias", connection);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            var response = new List<Colonia>();
            try
            {
                await connection.OpenAsync();
                var reader = await cmd.ExecuteReaderAsync();
                while (await reader.ReadAsync())
                {
                    response.Add(MapToColonia(reader));
                }
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        public async Task<Colonia> AgregarColonia(Colonia colonia)
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_agregarColonia", connection);
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();
            var response = new Colonia();
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@nombre", colonia.nombre);
            try
            {
                await connection.OpenAsync();
                adapter.SelectCommand = cmd;
                adapter.Fill(dataSet);

                response = new Colonia()
                {
                    id = (int)dataSet.Tables[0].Rows[0].ItemArray[0],
                    nombre = dataSet.Tables[0].Rows[0].ItemArray[1].ToString()
                };
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        public async Task<Colonia> ActualizarColonia(Colonia colonia)
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_actualizarColonia", connection);
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();
            var response = new Colonia();
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", colonia.id);
            cmd.Parameters.AddWithValue("@nombre", colonia.nombre);
            try
            {
                await connection.OpenAsync();
                adapter.SelectCommand = cmd;
                adapter.Fill(dataSet);

                response = new Colonia()
                {
                    id = (int)dataSet.Tables[0].Rows[0].ItemArray[0],
                    nombre = dataSet.Tables[0].Rows[0].ItemArray[1].ToString()
                };
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        private Colonia MapToColonia(SqlDataReader reader)
        {
            return new Colonia()
            {
                id = (int)reader["id"],
                nombre = reader["nombre"].ToString()
            };
        }
    }
}
