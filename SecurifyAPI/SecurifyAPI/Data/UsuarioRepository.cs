﻿using Microsoft.Extensions.Configuration;
using SecurifyAPI.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SecurifyAPI.Data
{
    public class UsuarioRepository
    {
        private readonly string _connectionString;

        public UsuarioRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("defaultConnection");
        }

        public async Task<List<Usuario>> GetUsuarios()
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_getUsuarios", connection);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            var response = new List<Usuario>();
            try
            {
                await connection.OpenAsync();
                var reader = await cmd.ExecuteReaderAsync();
                while (await reader.ReadAsync())
                {
                    response.Add(MapToUsuario(reader));
                }
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        public async Task<List<Usuario>> GetUsuariosNombre(Usuario usuario)
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_getUsuariosNombre", connection);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@nombre", usuario.nombre);
            var response = new List<Usuario>();
            try
            {
                await connection.OpenAsync();
                var reader = await cmd.ExecuteReaderAsync();
                while (await reader.ReadAsync())
                {
                    response.Add(MapToUsuario(reader));
                }
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        public async Task<Usuario> LoginUsuario(Usuario usuario)
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_loginUsuario", connection);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codigo", usuario.codigo);
            string pass = Encrypt.GetSHA256(usuario.password);
            cmd.Parameters.AddWithValue("@password", pass );
            List<Usuario> usuarios = new List<Usuario>();
            var response = new Usuario();
            try
            {
                await connection.OpenAsync();
                var reader = await cmd.ExecuteReaderAsync();
                while (await reader.ReadAsync())
                {
                    usuarios.Add(MapToUsuario(reader));
                }
                response = usuarios[0];
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        public async Task<Usuario> GetUsuarioID(Usuario usuario)
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_getUsuarioID", connection);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", usuario.id);
            List<Usuario> usuarios = new List<Usuario>();
            var response = new Usuario();
            try
            {
                await connection.OpenAsync();
                var reader = await cmd.ExecuteReaderAsync();
                while (await reader.ReadAsync())
                {
                    usuarios.Add(MapToUsuario(reader));
                }
                response = usuarios[0];
            }
            catch (Exception ex)
            {

            }
            return response;
        }


        public async Task<Usuario> AgregarUsuario(Usuario usuario)
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_agregarUsuario", connection);
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();
            var response = new Usuario();
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idPerfil", usuario.idPerfil);
            cmd.Parameters.AddWithValue("@idDomicilio", usuario.idDomicilio);
            cmd.Parameters.AddWithValue("@codigo", usuario.codigo);
            cmd.Parameters.AddWithValue("@password", Encrypt.GetSHA256(usuario.password));
            cmd.Parameters.AddWithValue("@nombre", usuario.nombre);
            cmd.Parameters.AddWithValue("@temporal", usuario.temporal);
            cmd.Parameters.AddWithValue("@status", usuario.status);
            try
            {
                await connection.OpenAsync();
                adapter.SelectCommand = cmd;
                adapter.Fill(dataSet);

                response = new Usuario()
                {
                    id = (int)dataSet.Tables[0].Rows[0].ItemArray[0],
                    idPerfil = (int)dataSet.Tables[0].Rows[0].ItemArray[1],
                    idDomicilio = (int)dataSet.Tables[0].Rows[0].ItemArray[2],
                    codigo = dataSet.Tables[0].Rows[0].ItemArray[3].ToString(),
                    password = dataSet.Tables[0].Rows[0].ItemArray[4].ToString(),
                    nombre = dataSet.Tables[0].Rows[0].ItemArray[5].ToString(),
                    temporal = dataSet.Tables[0].Rows[0].ItemArray[6].ToString(),
                    status = (int)dataSet.Tables[0].Rows[0].ItemArray[7]
                };
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        public async Task<Usuario> ActualizarUsuario(Usuario usuario)
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_actualizarUsuario", connection);
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();
            var response = new Usuario();
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", usuario.id);
            cmd.Parameters.AddWithValue("@idPerfil", usuario.idPerfil);
            cmd.Parameters.AddWithValue("@idDomicilio", usuario.idDomicilio);
            cmd.Parameters.AddWithValue("@codigo", usuario.codigo);
            cmd.Parameters.AddWithValue("@password", usuario.password);
            cmd.Parameters.AddWithValue("@nombre", usuario.nombre);
            cmd.Parameters.AddWithValue("@temporal", usuario.temporal);
            cmd.Parameters.AddWithValue("@status", usuario.status);
            try
            {
                await connection.OpenAsync();
                adapter.SelectCommand = cmd;
                adapter.Fill(dataSet);

                response = new Usuario()
                {
                    id = (int)dataSet.Tables[0].Rows[0].ItemArray[0],
                    idPerfil = (int)dataSet.Tables[0].Rows[0].ItemArray[1],
                    idDomicilio = (int)dataSet.Tables[0].Rows[0].ItemArray[2],
                    codigo = dataSet.Tables[0].Rows[0].ItemArray[3].ToString(),
                    password = dataSet.Tables[0].Rows[0].ItemArray[4].ToString(),
                    nombre = dataSet.Tables[0].Rows[0].ItemArray[5].ToString(),
                    temporal = dataSet.Tables[0].Rows[0].ItemArray[6].ToString(),
                    status = (int)dataSet.Tables[0].Rows[0].ItemArray[7]
                };
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        private Usuario MapToUsuario(SqlDataReader reader)
        {
            return new Usuario()
            {
                id = (int)reader["id"],
                idPerfil = (int)reader["idPerfil"],
                idDomicilio = (int)reader["idDomicilio"],
                codigo = reader["codigo"].ToString(),
                password = reader["password"].ToString(),
                nombre = reader["nombre"].ToString(),
                temporal = reader["temporal"].ToString(),
                status = (int)reader["status"],
            };
        }
    }
}
