﻿using Microsoft.Extensions.Configuration;
using SecurifyAPI.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SecurifyAPI.Data
{
    public class VisitanteRepository
    {
        private readonly string _connectionString;

        public VisitanteRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("defaultConnection");
        }

        public async Task<List<Visitante>> GetVisitantes()
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_getVisitantes", connection);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            var response = new List<Visitante>();
            try
            {
                await connection.OpenAsync();
                var reader = await cmd.ExecuteReaderAsync();
                while (await reader.ReadAsync())
                {
                    response.Add(MapToVisitante(reader));
                }
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        public async Task<Visitante> AgregarVisitante(Visitante visitante)
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_agregarVisitante", connection);
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();
            var response = new Visitante();
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@nombre", visitante.nombre);
            cmd.Parameters.AddWithValue("@status", visitante.nombre);
            try
            {
                await connection.OpenAsync();
                adapter.SelectCommand = cmd;
                adapter.Fill(dataSet);

                response = new Visitante()
                {
                    id = (int)dataSet.Tables[0].Rows[0].ItemArray[0],
                    nombre = dataSet.Tables[0].Rows[0].ItemArray[1].ToString(),
                    status = (int)dataSet.Tables[0].Rows[0].ItemArray[2]
                };
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        public async Task<Visitante> ActualizarVisitante(Visitante visitante)
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_actualizarVisitante", connection);
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();
            var response = new Visitante();
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", visitante.id);
            cmd.Parameters.AddWithValue("@nombre", visitante.nombre);
            cmd.Parameters.AddWithValue("@status", visitante.nombre);
            try
            {
                await connection.OpenAsync();
                adapter.SelectCommand = cmd;
                adapter.Fill(dataSet);

                response = new Visitante()
                {
                    id = (int)dataSet.Tables[0].Rows[0].ItemArray[0],
                    nombre = dataSet.Tables[0].Rows[0].ItemArray[1].ToString(),
                    status = (int)dataSet.Tables[0].Rows[0].ItemArray[2]
                };
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        public async Task<bool> BloquearVisitante(Visitante visitante)
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_bloquearVisitante", connection);
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();
            var response = false;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", visitante.id);
            cmd.Parameters.AddWithValue("@status", visitante.nombre);
            try
            {
                await connection.OpenAsync();
                await cmd.ExecuteNonQueryAsync();
                response = true;
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        private Visitante MapToVisitante(SqlDataReader reader)
        {
            return new Visitante()
            {
                id = (int)reader["id"],
                nombre = reader["nombre"].ToString(),
                status = (int)reader["status"]
            };
        }
    }
}
