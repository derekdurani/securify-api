﻿using Microsoft.Extensions.Configuration;
using SecurifyAPI.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SecurifyAPI.Data
{
    public class CalleRepository
    {
        private readonly string _connectionString;

        public CalleRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("defaultConnection");
        }

        public async Task<List<Calle>> GetCalles()
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_getCalles", connection);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            var response = new List<Calle>();
            try
            {
                await connection.OpenAsync();
                var reader = await cmd.ExecuteReaderAsync();
                while (await reader.ReadAsync())
                {
                    response.Add(MapToCalle(reader));
                }
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        public async Task<Calle> AgregarCalle(Calle calle)
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_agregarCalle", connection);
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();
            var response = new Calle();
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idColonia", calle.idColonia);
            cmd.Parameters.AddWithValue("@nombre", calle.nombre);
            try
            {
                await connection.OpenAsync();
                adapter.SelectCommand = cmd;
                adapter.Fill(dataSet);

                response = new Calle()
                {
                    id = (int)dataSet.Tables[0].Rows[0].ItemArray[0],
                    idColonia = (int)dataSet.Tables[0].Rows[0].ItemArray[1],
                    nombre = dataSet.Tables[0].Rows[0].ItemArray[2].ToString()
                };
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        public async Task<Calle> ActualizarCalle(Calle calle)
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("sp_actualizarCalle", connection);
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();
            var response = new Calle();
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", calle.id);
            cmd.Parameters.AddWithValue("@idColonia", calle.idColonia);
            cmd.Parameters.AddWithValue("@nombre", calle.nombre);
            try
            {
                await connection.OpenAsync();
                adapter.SelectCommand = cmd;
                adapter.Fill(dataSet);

                response = new Calle()
                {
                    id = (int)dataSet.Tables[0].Rows[0].ItemArray[0],
                    idColonia = (int)dataSet.Tables[0].Rows[0].ItemArray[1],
                    nombre = dataSet.Tables[0].Rows[0].ItemArray[2].ToString()
                };
            }
            catch (Exception ex)
            {

            }
            return response;
        }



        private Calle MapToCalle(SqlDataReader reader)
        {
            return new Calle()
            {
                id = (int)reader["id"],
                idColonia = (int)reader["idColonia"],
                nombre = reader["nombre"].ToString()
            };
        }
    }
}
