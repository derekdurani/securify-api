﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecurifyAPI.Model
{
    public class Visita
    {
        public int id { get; set; }
        public int idUsuario { get; set; }
        public int idVisitante { get; set; }
        public int idDomicilio { get; set; }
        public int idTipo { get; set; }
        public DateTime fecha { get; set; }
    }
}
