﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecurifyAPI.Model
{
    public class Perfil
    {
        public int id { get; set; }
        public string nombre { get; set; }
    }
}
