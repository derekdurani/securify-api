﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecurifyAPI.Model
{
    public class Usuario
    {
        public int id { get; set; }
        public int idPerfil { get; set; }
        public int idDomicilio { get; set; }
        public string codigo { get; set; }
        public string password { get; set; }
        public string nombre { get; set; }
        public string temporal { get; set; }
        public int status { get; set; }
    }
}
