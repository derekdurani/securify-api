﻿using Microsoft.AspNetCore.Mvc;
using SecurifyAPI.Data;
using SecurifyAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecurifyAPI.Controllers
{
    [ApiController]
    public class TipoController : ControllerBase
    {

        private readonly TipoRepository _repository;

        public TipoController(TipoRepository repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }
        // GET api/visitas

        [HttpGet]
        [Route("api/[controller]")]
        public async Task<List<Tipo>> Get()
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw;
            }
            return await _repository.GetTipos();
        }


        [HttpPost]
        [Route("api/[controller]/agregar")]
        public async Task<Tipo> Insert(Tipo tipo)
        {
            return await _repository.AgregarTipo(tipo);
        }

        [HttpPut]
        [Route("api/[controller]/actualizar")]
        public async Task<Tipo> Update(Tipo tipo)
        {
            return await _repository.ActualizarTipo(tipo);
        }

    }
}
