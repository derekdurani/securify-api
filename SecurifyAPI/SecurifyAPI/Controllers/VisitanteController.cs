﻿using Microsoft.AspNetCore.Mvc;
using SecurifyAPI.Data;
using SecurifyAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecurifyAPI.Controllers
{
    [ApiController]
    public class VisitanteController : ControllerBase
    {

        private readonly VisitanteRepository _repository;

        public VisitanteController(VisitanteRepository repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }
        // GET api/visitas

        [HttpGet]
        [Route("api/[controller]")]
        public async Task<List<Visitante>> Get()
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw;
            }
            return await _repository.GetVisitantes();
        }


        [HttpPost]
        [Route("api/[controller]/agregar")]
        public async Task<Visitante> Insert(Visitante visitante)
        {
            return await _repository.AgregarVisitante(visitante);
        }

        [HttpPut]
        [Route("api/[controller]/actualizar")]
        public async Task<Visitante> Update(Visitante visitante)
        {
            return await _repository.ActualizarVisitante(visitante);
        }

        [HttpPut]
        [Route("api/[controller]/bloquear")]
        public async Task<bool> Block(Visitante visitante)
        {
            return await _repository.BloquearVisitante(visitante);
        }
    }
}
