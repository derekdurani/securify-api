﻿using Microsoft.AspNetCore.Mvc;
using SecurifyAPI.Data;
using SecurifyAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecurifyAPI.Controllers
{
    [ApiController]
    public class ColoniaController : ControllerBase
    {

        private readonly ColoniaRepository _repository;

        public ColoniaController(ColoniaRepository repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }
        // GET api/visitas

        [HttpGet]
        [Route("api/[controller]")]
        public async Task<List<Colonia>> Get()
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw;
            }
            return await _repository.GetColonias();
        }


        [HttpPost]
        [Route("api/[controller]/agregar")]
        public async Task<Colonia> Insert(Colonia colonia)
        {
            return await _repository.AgregarColonia(colonia);
        }

        [HttpPut]
        [Route("api/[controller]/actualizar")]
        public async Task<Colonia> Update(Colonia colonia)
        {
            return await _repository.ActualizarColonia(colonia);
        }

    }
}
