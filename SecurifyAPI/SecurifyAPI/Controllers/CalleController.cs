﻿using Microsoft.AspNetCore.Mvc;
using SecurifyAPI.Data;
using SecurifyAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecurifyAPI.Controllers
{
    [ApiController]
    public class CalleController : ControllerBase
    {

        private readonly CalleRepository _repository;

        public CalleController(CalleRepository repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }
        // GET api/visitas

        [HttpGet]
        [Route("api/[controller]")]
        public async Task<List<Calle>> Get()
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw;
            }
            return await _repository.GetCalles();
        }


        [HttpPost]
        [Route("api/[controller]/agregar")]
        public async Task<Calle> Insert(Calle calle)
        {
            return await _repository.AgregarCalle(calle);
        }

        [HttpPut]
        [Route("api/[controller]/actualizar")]
        public async Task<Calle> Update(Calle calle)
        {
            return await _repository.ActualizarCalle(calle);
        }

    }
}
