﻿using Microsoft.AspNetCore.Mvc;
using SecurifyAPI.Data;
using SecurifyAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecurifyAPI.Controllers
{
    [ApiController]
    public class DomicilioController : ControllerBase
    {

        private readonly DomicilioRepository _repository;

        public DomicilioController(DomicilioRepository repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }
        // GET api/visitas

        [HttpGet]
        [Route("api/[controller]")]
        public async Task<List<Domicilio>> Get()
        {
            return await _repository.GetDomicilios();
        }
        
        [HttpPost]
        [Route("api/[controller]/agregar")]
        public async Task<Domicilio> Insert(Domicilio domicilio)
        {
            return await _repository.AgregarDomicilio(domicilio);
        }

        [HttpPut]
        [Route("api/[controller]/actualizar")]
        public async Task<Domicilio> Update(Domicilio domicilio)
        {
            return await _repository.ActualizarDomicilio(domicilio);
        }

        [HttpPost]
        [Route("api/[controller]/getid")]
        public async Task<Domicilio> GetID(Domicilio domicilio)
        {
            return await _repository.GetDomicilioID(domicilio);
        }
    }
}
