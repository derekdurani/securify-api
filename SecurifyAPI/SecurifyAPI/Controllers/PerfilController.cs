﻿using Microsoft.AspNetCore.Mvc;
using SecurifyAPI.Data;
using SecurifyAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecurifyAPI.Controllers
{
    [ApiController]
    public class PerfilController : ControllerBase
    {

        private readonly PerfilRepository _repository;

        public PerfilController(PerfilRepository repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }
        // GET api/visitas
       
        [HttpGet]
        [Route("api/[controller]")]
        public async Task<List<Perfil>> Get()
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw;
            }
            return await _repository.GetPerfiles();
        }

        
        [HttpPost]
        [Route("api/[controller]/agregar")]
        public async Task<Perfil> Insert(Perfil perfil)
        {
            return await _repository.AgregarPerfil(perfil);
        }

        [HttpPut]
        [Route("api/[controller]/actualizar")]
        public async Task<Perfil> Update(Perfil perfil)
        {
            return await _repository.ActualizarPerfil(perfil);
        }

    }
}
