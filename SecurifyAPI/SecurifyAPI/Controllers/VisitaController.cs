﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SecurifyAPI.Data;
using SecurifyAPI.Model;

namespace SecurifyAPI.Controllers
{
    
    [ApiController]
    public class VisitaController : ControllerBase
    {

        private readonly VisitaRepository _repository;

        public VisitaController(VisitaRepository repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }
        // GET api/visitas
        [Route("api/[controller]")]
        [HttpGet]
        public async Task<List<Visita>> Get()
        {
            return await _repository.GetVisitas();
        }
        
        [Route("api/[controller]/agregar")]
        [HttpPost]
        public async Task<Visita> Insert(Visita visita)
        {
            return await _repository.AgregarVisita(visita);
        }

    }
}
