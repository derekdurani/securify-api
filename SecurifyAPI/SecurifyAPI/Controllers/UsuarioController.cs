﻿using Microsoft.AspNetCore.Mvc;
using SecurifyAPI.Data;
using SecurifyAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecurifyAPI.Controllers
{
   [ApiController]
    public class UsuarioController : ControllerBase
    {

        private readonly UsuarioRepository _repository;

        public UsuarioController(UsuarioRepository repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }
        // GET api/visitas

        [HttpGet]
        [Route("api/[controller]")]
        public async Task<List<Usuario>> Get()
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw;
            }
            return await _repository.GetUsuarios();
        }


        [HttpPost]
        [Route("api/[controller]/agregar")]
        public async Task<Usuario> Insert(Usuario usuario)
        {
            return await _repository.AgregarUsuario(usuario);
        }

        [HttpPost]
        [Route("api/[controller]/login")]
        public async Task<Usuario> Login(Usuario usuario)
        {
            return await _repository.LoginUsuario(usuario);
        }

        [HttpPost]
        [Route("api/[controller]/getnombre")]
        public async Task<List<Usuario>> GetNombre(Usuario usuario)
        {
            return await _repository.GetUsuariosNombre(usuario);
        }

        [HttpPost]
        [Route("api/[controller]/getid")]
        public async Task<Usuario> GetID(Usuario usuario)
        {
            return await _repository.GetUsuarioID(usuario);
        }

        [HttpPut]
        [Route("api/[controller]/actualizar")]
        public async Task<Usuario> Update(Usuario usuario)
        {
            return await _repository.ActualizarUsuario(usuario);
        }

    }
}
